import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import HelpPage from './HelpPage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the help page', () => {
  test('rendering the help page', () => {
    const wrapper = shallowMount(HelpPage, {
      global: {
        components: {
          HelpPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="help-page"]').exists()).toBe(true)
  })

  test('testing help page UI', () => {
    const wrapper = shallowMount(HelpPage, {
      global: {
        components: {
          HelpPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
