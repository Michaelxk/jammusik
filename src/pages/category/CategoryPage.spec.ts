import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import CategoryPage from './CategoryPage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the category page', () => {
  test('rendering the category page', () => {
    const wrapper = shallowMount(CategoryPage, {
      global: {
        components: {
          CategoryPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="category-page"]').exists()).toBe(true)
  })

  test('testing category page UI', () => {
    const wrapper = shallowMount(CategoryPage, {
      global: {
        components: {
          CategoryPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
