import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import ProfilePage from './ProfilePage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the profile page', () => {
  test('rendering the profile page', () => {
    const wrapper = shallowMount(ProfilePage, {
      global: {
        components: {
          ProfilePage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="profile-page"]').exists()).toBe(true)
  })

  test('testing profile page UI', () => {
    const wrapper = shallowMount(ProfilePage, {
      global: {
        components: {
          ProfilePage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
