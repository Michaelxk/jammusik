import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import PlaylistPage from './PlaylistPage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the playlist page', () => {
  test('rendering the playlist page', () => {
    const wrapper = shallowMount(PlaylistPage, {
      global: {
        components: {
          PlaylistPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="playlist-page"]').exists()).toBe(true)
  })

  test('testing playlist page UI', () => {
    const wrapper = shallowMount(PlaylistPage, {
      global: {
        components: {
          PlaylistPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
