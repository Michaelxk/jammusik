import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import PathPickPage from './PathPickPage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the path pick page', () => {
  test('rendering the path pick page', () => {
    const wrapper = shallowMount(PathPickPage, {
      global: {
        components: {
          PathPickPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="path-pick-page"]').exists()).toBe(true)
  })

  test('testing path pick page UI', () => {
    const wrapper = shallowMount(PathPickPage, {
      global: {
        components: {
          PathPickPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
