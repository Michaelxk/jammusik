import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import HomePage from './HomePage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the home page', () => {
  test('rendering the home page', () => {
    const wrapper = shallowMount(HomePage, {
      global: {
        components: {
          HomePage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="home-page"]').exists()).toBe(true)
  })

  test('testing home page UI', () => {
    const wrapper = shallowMount(HomePage, {
      global: {
        components: {
          HomePage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
