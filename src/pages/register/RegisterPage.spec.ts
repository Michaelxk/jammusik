import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import RegisterPage from './RegisterPage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the register page', () => {
  test('rendering the register page', () => {
    const wrapper = shallowMount(RegisterPage, {
      global: {
        components: {
          RegisterPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="register-page"]').exists()).toBe(true)
  })

  test('testing register page UI', () => {
    const wrapper = shallowMount(RegisterPage, {
      global: {
        components: {
          RegisterPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
