import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import LoginPage from './LoginPage.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the login page', () => {
  test('rendering the login page', () => {
    const wrapper = shallowMount(LoginPage, {
      global: {
        components: {
          LoginPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="login-page"]').exists()).toBe(true)
  })

  test('testing login page UI', () => {
    const wrapper = shallowMount(LoginPage, {
      global: {
        components: {
          LoginPage
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
