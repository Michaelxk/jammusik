import { createRouter, createWebHistory } from 'vue-router';
import PathPickPage from '../pages/PathPickPage.vue'
import { auth } from '@/infra/api/firebaseConfig';

const requiredAuth = (to: Object, from: Object , next: Function) => {
  const user = auth.currentUser
  if(!user) {
    next({ name: 'Login' })
  }else {
    next()
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'PathPickPage',
      component: PathPickPage
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('../pages/login/LoginPage.vue')
    },
    {
      path: '/register',
      name: 'RegisterPage',
      component: () => import('../pages/register/RegisterPage.vue')
    },
    {
      path: '/homepage',
      name: 'HomePage',
      component: () => import('../pages/home/HomePage.vue'),
      beforeEnter: requiredAuth
    },
    {
      path: '/playlist',
      name: 'PlaylistPage',
      component: () => import('../pages/playlist/PlaylistPage.vue'),
      beforeEnter: requiredAuth
    },
    {
      path: '/playlist/:id/:title',
      name: 'PlaylistSelected',
      component: () => import('../pages/playlist/PlaylistSelected.vue'),
      beforeEnter: requiredAuth,
      props: true
    },
    {
      path: '/category',
      name: 'CategoryPage',
      component: () => import('../pages/category/CategoryPage.vue'),
      beforeEnter: requiredAuth
    },
    {
      path: '/category/:id/:title',
      name: 'CategorySelected',
      component: () => import('../pages/category/CategorySelected.vue'),
      beforeEnter: requiredAuth,
      props: true
    },
    {
      path: '/profile',
      name: 'Profile',
      component: () => import('../pages/profile/ProfilePage.vue'),
      beforeEnter: requiredAuth
    },
    {
      path: '/playlist/:playlistId/song/:songId',
      name: 'SongSelected',
      component: () => import('../pages/song/SongSelected.vue'),
      beforeEnter: requiredAuth,
      props: true
    },
    {
      path: '/help',
      name: 'HelpPage',
      component: () => import('@/pages/help/HelpPage.vue'),
      beforeEnter: requiredAuth
    },
    {
      path: '/feedback',
      name: 'FeedBack',
      component: () => import('@/pages/feedback/Feedback.vue'),
      beforeEnter: requiredAuth
    }
  ]
})

export default router
