import './assets/main.css'

import { createApp, type App as VueApp } from 'vue'
import App from './App.vue'

import { registerPlugins } from './plugins'
import { auth } from '@/infra/api/firebaseConfig';

let app: VueApp | undefined;

auth.onAuthStateChanged(() => {
  if(!app) {
    app = createApp(App)
    registerPlugins(app)
    app.mount('#app')
  }
})

