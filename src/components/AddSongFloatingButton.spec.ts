import { mount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import AddSongFloatingButton from './AddSongFloatingButton.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the add song floating button component', () => {
  test('rendering the add song floating button component', () => {
    const wrapper = mount(AddSongFloatingButton, {
      global: {
        components: {
          AddSongFloatingButton
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="add-song-button"]').exists()).toBe(true)
  })

  test('testing the add song floating button UI', () => {
    const wrapper = mount(AddSongFloatingButton, {
      global: {
        components: {
          AddSongFloatingButton
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })

  test('testig menu toggle button click', async () => {
    const wrapper = mount(AddSongFloatingButton, {
      global: {
        components: {
          AddSongFloatingButton
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="add-song-button"]').exists()).toBe(true)
    await wrapper.get('[data-test="add-song-button"]').trigger('click')
    expect(wrapper.emitted()).toBeTruthy()
  })
})
