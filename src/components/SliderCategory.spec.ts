import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import SliderCategory from './SliderCategory.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the slider category component', () => {
  test('rendering the slider category component', () => {
    const wrapper = shallowMount(SliderCategory, {
      global: {
        components: {
          SliderCategory
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="slider-category"]').exists()).toBe(true)
  })

  test('testing the slider category UI', () => {
    const wrapper = shallowMount(SliderCategory, {
      global: {
        components: {
          SliderCategory
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
