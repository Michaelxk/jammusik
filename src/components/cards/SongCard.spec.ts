import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import SongCard from './SongCard.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the song card component', () => {
  test('rendering the song card component', async () => {
    const wrapper = shallowMount(SongCard, {
      global: {
        components: {
          SongCard
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="song-card"]').exists()).toBe(true)
  })

  test('testing array of songList as props in song card component', () => {
    const songList = [
      { id: '1', title: 'rap', artist: 'pedro' },
      { id: '2', title: 'rock', artist: 'ramon' },
      { id: '3', title: 'pop', artist: 'andres' }
    ]
    const wrapper = shallowMount(SongCard, {
      propsData: {
        songList,
        songsCounter: songList.length
      },
      global: {
        components: {
          SongCard
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.props().songList).toEqual(songList)
  })

  test('testing song card component UI', () => {
    const wrapper = shallowMount(SongCard, {
      global: {
        components: {
          SongCard
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
