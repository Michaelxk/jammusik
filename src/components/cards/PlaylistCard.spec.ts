import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import PlaylistCard from './PlaylistCard.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the playlist card component', () => {
  test('rendering the playlist component', async () => {
    const wrapper = shallowMount(PlaylistCard, {
      global: {
        components: {
          PlaylistCard
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="playlist-card"]').exists()).toBe(true)
  })

  test('testing array of playlist as props in playlist card component', () => {
    const playlists = [
      { id: '1', title: 'nice music', categoryId: '1' },
      { id: '2', title: 'the best', categoryId: '2' },
      { id: '3', title: 'the last', categoryId: '3' }
    ]
    const wrapper = shallowMount(PlaylistCard, {
      propsData: {
        playlists: []
      },
      global: {
        components: {
          PlaylistCard
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.props().playlists).toEqual(playlists)
  })

  test('testing playlist card component UI', () => {
    const wrapper = shallowMount(PlaylistCard, {
      global: {
        components: {
          PlaylistCard
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
