import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import CategoryCard from './CategoryCard.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the category card component', () => {
  test('rendering the category card component', async () => {
    const wrapper = shallowMount(CategoryCard, {
      global: {
        components: {
          CategoryCard
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="category-card"]').exists()).toBe(true)
  })

  test('testing array of categories as props in category card component', () => {
    const categories = [
      { id: '1', title: 'rap' },
      { id: '2', title: 'rock' },
      { id: '3', title: 'pop' }
    ]
    const wrapper = shallowMount(CategoryCard, {
      propsData: {
        categories: []
      },
      global: {
        components: {
          CategoryCard
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.props().categories).toEqual(categories)
  })

  test('testing category card component UI', () => {
    const wrapper = shallowMount(CategoryCard, {
      global: {
        components: {
          CategoryCard
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
