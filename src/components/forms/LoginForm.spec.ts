import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import LoginForm from './LoginForm.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the login form component', () => {
  test('rendering the login form component', () => {
    const wrapper = shallowMount(LoginForm, {
      global: {
        components: {
          LoginForm
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="login-form"]').exists()).toBe(true)
  })

  test('testing the login form UI', () => {
    const wrapper = shallowMount(LoginForm, {
      global: {
        components: {
          LoginForm
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
