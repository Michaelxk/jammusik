import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import ProfileForm from './ProfileForm.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the rofile form component', () => {
  test('rendering the profile form component', () => {
    const wrapper = shallowMount(ProfileForm, {
      global: {
        components: {
          ProfileForm
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="profile-form"]').exists()).toBe(true)
  })

  test('testing the profile form UI', () => {
    const wrapper = shallowMount(ProfileForm, {
      global: {
        components: {
          ProfileForm
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
