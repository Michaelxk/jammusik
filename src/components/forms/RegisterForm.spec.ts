import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import RegisterForm from './RegisterForm.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the register form component', () => {
  test('rendering the register form component', () => {
    const wrapper = shallowMount(RegisterForm, {
      global: {
        components: {
          RegisterForm
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="register-form"]').exists()).toBe(true)
  })

  test('testing the register form UI', () => {
    const wrapper = shallowMount(RegisterForm, {
      global: {
        components: {
          RegisterForm
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
