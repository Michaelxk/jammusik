import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import AuthInvitation from './AuthInvitation.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the auth invitation component', () => {
  test('rendering the auth invitation component', () => {
    const wrapper = shallowMount(AuthInvitation, {
      global: {
        components: {
          AuthInvitation
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="auth-invitation"]').exists()).toBe(true)
  })

  test('testing the auth invitation UI', () => {
    const wrapper = shallowMount(AuthInvitation, {
      global: {
        components: {
          AuthInvitation
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
