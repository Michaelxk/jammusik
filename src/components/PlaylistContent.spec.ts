import { shallowMount } from '@vue/test-utils'
import { test, expect, describe, beforeAll } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'
import vuetify from '../plugins/vuetify'
import PlaylistContent from './PlaylistContent.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the playlist content component', () => {
  beforeAll(() => {
    setActivePinia(createPinia())
  })
  test('rendering the playlist content component', () => {
    const wrapper = shallowMount(PlaylistContent, {
      global: {
        components: {
          PlaylistContent
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="playlist-content"]').exists()).toBe(true)
  })

  test('testing the playlist content UI', () => {
    const wrapper = shallowMount(PlaylistContent, {
      global: {
        components: {
          PlaylistContent
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
