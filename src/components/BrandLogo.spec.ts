import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import BrandLogo from './BrandLogo.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the brand logo component', () => {
  test('rendering the brand logo component', () => {
    const wrapper = shallowMount(BrandLogo, {
      global: {
        components: {
          BrandLogo
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="brand-logo"]').exists()).toBe(true)
  })

  test('testing the brand logo UI', () => {
    const wrapper = shallowMount(BrandLogo, {
      global: {
        components: {
          BrandLogo
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
