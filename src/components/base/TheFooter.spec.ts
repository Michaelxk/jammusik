import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import TheFoooter from './TheFooter.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the footer component', () => {
  test('rendering the footer component', () => {
    const wrapper = shallowMount(TheFoooter, {
      global: {
        components: {
          TheFoooter
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="footer rendered"]').exists()).toBe(true)
  })

  test('testing the footer UI', () => {
    const wrapper = shallowMount(TheFoooter, {
      global: {
        components: {
          TheFoooter
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
