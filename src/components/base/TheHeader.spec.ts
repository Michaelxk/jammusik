import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import TheHeader from './TheHeader.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the header component', () => {
  test('rendering the header component', () => {
    const wrapper = shallowMount(TheHeader, {
      global: {
        components: {
          TheHeader
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="header"]').exists()).toBe(true)
  })

  test('testing header UI', () => {
    const wrapper = shallowMount(TheHeader, {
      global: {
        components: {
          TheHeader
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
