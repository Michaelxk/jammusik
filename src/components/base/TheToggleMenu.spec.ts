import { shallowMount, mount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import TheToggleMenu from './TheToggleMenu.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the toggle menu component', () => {
  test('rendering the toggle menu button component', () => {
    const wrapper = shallowMount(TheToggleMenu, {
      global: {
        components: {
          TheToggleMenu
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="toggle menu"]').exists()).toBe(true)
  })

  test('testing toggle menu button UI', () => {
    const wrapper = shallowMount(TheToggleMenu, {
      global: {
        components: {
          TheToggleMenu
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })

  test('testig menu toggle button click', async () => {
    const wrapper = mount(TheToggleMenu, {
      global: {
        components: {
          TheToggleMenu
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="menu-button"]').exists()).toBe(true)
    await wrapper.get('[data-test="menu-button"]').trigger('click')
    expect(wrapper.emitted()).toBeTruthy()
  })
})
