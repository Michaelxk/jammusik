import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import TheAppBar from './TheAppBar.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the appbar component', () => {
  test('rendering the appbar component', async () => {
    const wrapper = shallowMount(TheAppBar, {
      global: {
        components: {
          TheAppBar
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="appbar"]').exists()).toBe(false)

    if (window.scrollY > 0) expect(wrapper.find('[data-test="appbar"]').exists()).toBe(true)
  })

  test('testing app bar UI', () => {
    const wrapper = shallowMount(TheAppBar, {
      global: {
        components: {
          TheAppBar
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
