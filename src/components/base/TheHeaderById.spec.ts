import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import TheHeaderById from './TheHeaderById.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the header by id component', () => {
  test('rendering the header by id component', () => {
    const wrapper = shallowMount(TheHeaderById, {
      global: {
        components: {
          TheHeaderById
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="header by id"]').exists()).toBe(true)
  })

  test('testing header by id UI', () => {
    const wrapper = shallowMount(TheHeaderById, {
      global: {
        components: {
          TheHeaderById
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })

  test('testing title prop', async () => {
    const title = 'title inital value'
    const updatedTile = 'title upadted value'
    const artist = 'artist inital value'
    const updatedArtist = 'artist updated value'

    const wrapper = shallowMount(TheHeaderById, {
      propsData: {
        title: title,
        artist: artist
      },
      global: {
        components: {
          TheHeaderById
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.props('title')).toBe(title)
    expect(wrapper.props('artist')).toBe(artist)

    await wrapper.setProps({
      title: updatedTile,
      artist: updatedArtist
    })

    expect(wrapper.props('title')).toBe(updatedTile)
    expect(wrapper.props('artist')).toBe(updatedArtist)
  })
})
