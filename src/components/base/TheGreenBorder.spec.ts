import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../../plugins/vuetify'
import TheGreenBorder from './TheGreenBorder.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the green border component', () => {
  test('rendering the green border component', () => {
    const wrapper = shallowMount(TheGreenBorder, {
      global: {
        components: {
          TheGreenBorder
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="green border"]').exists()).toBe(true)
  }),
    test('green border UI testing', () => {
      const wrapper = shallowMount(TheGreenBorder, {
        global: {
          components: {
            TheGreenBorder
          },
          plugins: [vuetify]
        }
      })

      expect(wrapper.text()).toMatchSnapshot()
    })
})
