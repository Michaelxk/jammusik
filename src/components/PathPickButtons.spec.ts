import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import PathPickButtons from './PathPickButtons.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the path pick buttons component', () => {
  test('rendering the path pick buttons component', () => {
    const wrapper = shallowMount(PathPickButtons, {
      global: {
        components: {
          PathPickButtons
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="path-pick-buttons"]').exists()).toBe(true)
  })

  test('testing the path pick buttons UI', () => {
    const wrapper = shallowMount(PathPickButtons, {
      global: {
        components: {
          PathPickButtons
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
