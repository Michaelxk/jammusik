import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import UserAvatar from './UserAvatar.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the user avatar component', () => {
  test('rendering the user avatar component', () => {
    const wrapper = shallowMount(UserAvatar, {
      global: {
        components: {
          UserAvatar
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="user-avatar"]').exists()).toBe(true)
  })

  test('testing the user avatar UI', () => {
    const wrapper = shallowMount(UserAvatar, {
      global: {
        components: {
          UserAvatar
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })
})
