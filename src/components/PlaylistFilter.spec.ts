import { shallowMount } from '@vue/test-utils'
import { test, expect, describe } from 'vitest'
import vuetify from '../plugins/vuetify'
import PlaylistFilter from './PlaylistFilter.vue'

global.ResizeObserver = require('resize-observer-polyfill')

describe('testing the playlist filter component', () => {
  test('rendering the playlist filter component', () => {
    const wrapper = shallowMount(PlaylistFilter, {
      global: {
        components: {
          PlaylistFilter
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.find('[data-test="playlist-filter"]').exists()).toBe(true)
  })

  test('testing the playlist filter UI', () => {
    const wrapper = shallowMount(PlaylistFilter, {
      global: {
        components: {
          PlaylistFilter
        },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toMatchSnapshot()
  })

  test('testig menu toggle button click', async () => {
    const wrapper = shallowMount(PlaylistFilter, {
      global: {
        components: {
          PlaylistFilter
        },
        plugins: [vuetify]
      }
    })

    expect(wrapper.find('[data-test="playlist-filter"]').exists()).toBe(true)
    await wrapper.get('[data-test="playlist-filter"]').trigger('click')
    expect(wrapper.emitted()).toBeTruthy()
  })
})
