/**
 * plugins/vuetify.ts
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
// import '@/styles/main.scss'

// Composables
import { createVuetify } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi'
import * as directives from 'vuetify/directives'
import * as components from 'vuetify/components'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi
    }
  },
  directives,
  components,

  theme: {
    defaultTheme: 'light',
    themes: {
      light: {
        dark: false,
        colors: {
          primary: '#070c0d',
          primaryAlt: '#0f1a1c',
          secondary: '#1b998b',
          secondaryAlt: '#6a888a',
          sotialFacebook: '#0389f7',
          sotialGoogle: '#e1a204',
          // color palette
          primaryColor: "#82B2A",
          primaryColorVar1: "#184945",
          primaryColorVar2: "#186D65",
          greyLight: "#848484",
          darkGrey: "#2b2b3c",
          greyColorVar1: "#f8f8f8",
          secondaryColor: "#18998B",
          secondaryColor2: "#22AA98",
          secondaryColorVar1: "#6ADEC9",
          secondaryColorVar2: "#A1EEDD",
          secondaryColorVar3: "#F0F7EE",
          lightColor: "#F1FCF9",
          lightColorVar1: "#DOFZEE",
        }
      },
      dark: {
        colors: {
          primary: '#FF4081',
          secondary: '#2195F3',
          textColor: '#fff',
          darkColor: '#191b31',
          lightBlue: '#dbe8f5',
        }
      }
    }
  }
})
