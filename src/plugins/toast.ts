import { toast, type ToastContainerOptions } from 'vue3-toastify'

const ToastConfigurations = {
  autoClose: 2300,
  position: toast.POSITION.BOTTOM_RIGHT
} as ToastContainerOptions

export { ToastConfigurations }
