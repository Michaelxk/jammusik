/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import vuetify from './vuetify'
import router from '../router'
import { createPinia } from 'pinia'
import { ToastConfigurations } from './toast'
import Vue3Toastify from 'vue3-toastify'

// Types
import type { App } from 'vue'

export function registerPlugins(app: App) {
  app.use(vuetify).use(router).use(createPinia()).use(Vue3Toastify, ToastConfigurations)
}
